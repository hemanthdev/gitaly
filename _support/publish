#!/usr/bin/env ruby

require_relative 'run.rb'

REMOTE_REGEX = %r{gitlab.com.gitlab-org/gitaly.git}

# Sanity check
%w[
  git@gitlab.com:gitlab-org/gitaly.git
  https://gitlab.com/gitlab-org/gitaly.git
  https://janedoe@gitlab.com/gitlab-org/gitaly.git
].each do |remote|
  abort "regex check failed failed for #{remote.inspect}" unless REMOTE_REGEX.match(remote)
end

def main(tag)
  remote = capture!(%w[git remote get-url --push origin])
  unless REMOTE_REGEX.match(remote)
    abort "Git remote 'origin' must match #{REMOTE_REGEX}, got #{remote.inspect}"
  end

  version = tag.sub(/^v/, '')

  unless version.match?(/\d+\.\d+\.\d+/)
    abort "Version string #{version.inspect} does not look like a Gitaly Release tag (e.g. \"v1.0.2\"). Aborting."
  end

  gem = "gitaly-#{version}.gem"
  abort "gem not found: #{gem}" unless File.exist?(gem)

  puts "Proceed to publish version #{tag}? Enter 'Yes' to continue; Ctrl-C to abort"
  $stdout.flush
  abort unless $stdin.gets.chomp == 'Yes'

  run!(%W[git push origin HEAD #{tag}])
  run!(%W[gem push #{gem}])

  # If this tag is not a patch release, we want a stable branch to be created
  return unless version.match?(/\.0\z/)

  stable_branch = "#{version.gsub(/\.0\z/, '').gsub('.', '-')}-stable"
  run!(%W[git branch #{stable_branch} #{tag}])
  run!(%W[git push -u origin #{stable_branch}])
end

unless ARGV.count == 1
  warn "Usage: #{$0} TAG"
  abort
end

main(ARGV[0])
