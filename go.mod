module gitlab.com/gitlab-org/gitaly

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/cloudflare/tableflip v0.0.0-20190329062924-8392f1641731
	github.com/getsentry/sentry-go v0.3.0
	github.com/golang/protobuf v1.3.2
	github.com/google/uuid v1.1.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/libgit2/git2go v0.0.0-20190104134018-ecaeb7a21d47
	github.com/prometheus/client_golang v1.0.0
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.4.0
	github.com/tinylib/msgp v1.1.0 // indirect
	gitlab.com/gitlab-org/labkit v0.0.0-20190221122536-0c3fc7cdd57c
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a
	google.golang.org/genproto v0.0.0-20181202183823-bd91e49a0898 // indirect
	google.golang.org/grpc v1.24.0
	gopkg.in/yaml.v2 v2.2.2
)

go 1.12
